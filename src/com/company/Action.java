package com.company;


import java.util.*;

public class Action {
    private final List<Cats> cats = new ArrayList<>();
    Scanner sc = new Scanner(System.in);

    public void startGame(){
        int days = 1;
        createCats();
        cats.sort(Comparator.comparing(Cats::getAverage).reversed());
        while (true){
            System.out.println("--------------------------------------- ДЕНЬ: "+days+"----------------------------------------");
            printTitle();
            cats.forEach(System.out::println);
            System.out.println("[1]: Кормить кота\n[2]: Играть с котом\n[3]: Лечить кота\n" +
                    "[4]: Следующий день\n[5]: Завести нового питомца\n" +
                    "[6]: Изменить сортировку таблицы\n[7]: Выйти\n");
            int choice = sc.nextInt();

            if (choice == 1){
                System.out.println("Выберите кота по номеру: (1-"+cats.size()+"): ");
                int cc = sc.nextInt();
                cats.get(cc -1).feed();
                System.out.println("Вы покормили "+cats.get(cc-1).getName()+"");
            }else if(choice == 2){
                System.out.println("Выберите кота по номеру: (1-"+cats.size()+"): ");
                int cc = sc.nextInt();
                cats.get(cc-1).playWithCat();
                System.out.println("Вы поиграли с "+cats.get(cc-1).getName()+"");
            }else if(choice == 3){
                System.out.println("Выберите кота по номеру: (1-"+cats.size()+"): ");
                int cc = sc.nextInt();
                cats.get(cc-1).treatCat();
                System.out.println("Вы полечили "+cats.get(cc-1).getName()+"");
            }else if(choice == 4){
                cats.get(cats.size()-1).nextDay();
                cats.get(1).nextDay();
                cats.get(2).nextDay();
                days++;
            }else if(choice == 5){
                Cats cat = Cats.createOne();
                cats.add(cat);
            }else if(choice == 6){
                System.out.println("Как хотите сделать сортировку?: ");
                System.out.println("1. По имени ");
                System.out.println("2. По возрасту ");
                System.out.println("3. По настроению ");
                System.out.println("4. По сытости ");
                System.out.println("5. По среднему уровню ");
                int a = sc.nextInt();
                if (a == 1){
                    cats.sort(Comparator.comparing(Cats::getName).reversed());
                }else if(a == 2){
                    cats.sort(Comparator.comparing(Cats::getAge).reversed());
                }else if(a == 3){
                    cats.sort(Comparator.comparing(Cats::getMood).reversed());
                }else if(a == 4){
                    cats.sort(Comparator.comparing(Cats::getSatiety).reversed());
                }else if(a == 5){
                    cats.sort(Comparator.comparing(Cats::getAverage).reversed());
                }else{
                    System.out.println("Выберите число от 1 до 5");
                }
            }else if(choice == 7){
                break;
            }
        }
        sc.close();
    }

    public void createCats(){
        int count = 1;
        while (true){
            Cats cat = Cats.initCats();
            cats.add(cat);
            if (count==3){
                break;
            }
            count++;
        }
    }

    private void printTitle() {
        System.out.println("---+-------------+-----------+------------+------------+------------+-----------------+\n"+
                " # +     ИМЯ     +  возраст  +  здоровье  + настроение +  сытость   + средний уровень +\n"+
                "---+-------------+-----------+------------+------------+------------+-----------------+");
    }

}
