package com.company;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Random;
import java.util.Scanner;

@Getter
@Setter
@AllArgsConstructor
public class Cats {
    private String name;
    private int age;
    private int health;
    private int mood;
    private int satiety;
    private double average;
    private static final Random r = new Random();
    private static final List<String> names = List.of("Peach", "Ginger", "Toby", "Seth", "Tibbles", "Tabby", "Poppy", "Millie", "Daisy", "Jasper", "Misty", "Minka");


    public static Cats initCats(){
        String name = names.get(r.nextInt(names.size()));
        int age = r.nextInt(18)+1;
        int health = r.nextInt(60)+20;
        int mood = r.nextInt(60)+20;
        int satiety = r.nextInt(60)+20;
        int average = (health+mood+satiety)/3;
        return new Cats(name, age, health, mood, satiety, average);
    }

    public static Cats createOne(){
        Scanner sc = new Scanner(System.in);
        Random r =new Random();
        System.out.print("Дайте имя коту: ");
        String name = sc.nextLine();
        System.out.print("Возраст кота? (1-18): ");
        int age = sc.nextInt();
        if (age>18 || age<1){
            System.out.println("Возраст может быть лишь между 1-18: Введите заново.");
            return createOne();
        }
        int health = r.nextInt(60)+20;
        int mood = r.nextInt(60)+20;
        int satiety = r.nextInt(60)+20;
        int average = (health+mood+satiety)/3;
        return new Cats(name, age, health, mood, satiety, average);
    }

    public void feed(){
        if (satiety>0 && health>0){
            int a = r.nextInt(7)+1;
            if (a == 1){
                System.out.println("Какой ужас, вы дали коту отравленную пищу!");
                mood = getMood() - 7;
                health = getHealth() - 10;
            }else {
                if(age>=1 && age<=5){
                    satiety = getSatiety() + 7;
                    mood = getMood() + 7;
                }else if(age>=6 && age<=10){
                    satiety = getSatiety() + 5;
                    mood = getMood() + 5;
                }else if(age>=11){
                    satiety = getSatiety() + 4;
                    mood = getMood() + 4;
                }
            }
        }else{
            System.out.println("Ваш кот умер смертью нехрабрых :(");
        }

        name = "* " + name;
    }

    public void playWithCat(){
        int a = r.nextInt(6)+1;
        if (a == 1){
            System.out.println("Какая криворукость, вы подвернули кошке лодыжку! Кот плачет!");
            health = getHealth() - 10;
            mood = getMood() - 10;
        }else {
            if(age>=1 && age<=5){
                health = getHealth() +7;
                satiety = getSatiety() -3;
                mood = getMood() + 7;
            }else if(age>=6 && age<=10){
                health = getHealth() +5;
                satiety = getSatiety() -5;
                mood = getMood() + 5;
            }else if(age>=11){
                health = getHealth() +4;
                satiety = getSatiety() -6;
                mood = getMood() + 4;
            }
        }
        name = "* " + name;
    }

    public void treatCat(){
        if(satiety>0 && health>0){
            int a = r.nextInt(6)+1;
            if (a == 1){
                System.out.println("Гореврач! Вы дали коту не то лекарство!");
                health = getHealth() - 15;
            }else{
                if(age>=1 && age<=5){
                    health = getHealth() +7;
                    satiety = getSatiety() -3;
                    mood = getMood() -3;
                }else if(age>=6 && age<=10){
                    health = getHealth() +5;
                    satiety = getSatiety() -5;
                    mood = getMood() -5;
                }else if(age>=11){
                    health = getHealth() +4;
                    satiety = getSatiety() -6;
                    mood = getMood() -6;
                }
            }
        }else{
            System.out.println("Ваш кот умер смертью нехрабрых :(");
        }

        name = "* " + name;
    }

    public void nextDay(){
        if (satiety>0 && health>0){
            satiety = getSatiety() -  (r.nextInt(5)+1);
            mood = getMood() - (r.nextInt(7)-3);
            health = getHealth() - (r.nextInt(7)-3);
        }else{
            System.out.println("Ваш кот умер смертью нехрабрых :(");
        }
    }



    @Override
    public String toString() {
        return String.format("   | %-11s | %-9s | %-10s | %-10s | %-10s | %-10s \n" +
                "---+-------------+-----------+------------+------------+------------+-----------------+"
                ,name, age, health, mood, satiety, average);
    }
}
